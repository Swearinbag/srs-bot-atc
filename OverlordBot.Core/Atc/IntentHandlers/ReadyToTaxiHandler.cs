﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections;
using NLog;
using RurouniJones.OverlordBot.Core.Atc.Util;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Atc.IntentHandlers
{
    internal class ReadyToTaxiHandler
    {
        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly Array InstructionsVariants =
            new ArrayList {"", "taxi to", "proceed to", "head to"}.ToArray();

        private static readonly Array ViaVariants = new ArrayList {"via", "along", "using"}.ToArray();

        private static readonly Random Randomizer = new();

        private static readonly TaxiRouter.TaxiInstructions DummyInstructions = new()
        {
            DestinationName = "Unknown Destination"
        };

        public static Reply Process(Unit transmitterUnit, AirfieldStatus airfield, Reply reply)
        {
            var taxiInstructions = DummyInstructions;

            try
            {
                taxiInstructions = TaxiRouter.Process(transmitterUnit.Location, airfield);

                var spokenInstructions = ConvertTaxiInstructionsToSsml(taxiInstructions);
                reply.Message = spokenInstructions;
                return reply;
            }
            catch (NoActiveRunwaysFoundException ex)
            {
                Logger.Error(ex, "No Active Runways found");
                reply.Message = "We could not find any active runways.";
                reply.ContinueProcessing = false;
                return reply;
            }
            catch (TaxiRouter.TaxiPathNotFoundException ex)
            {
                Logger.Error(ex, "No Path found");
                reply.Message = $"We could not find a path from your position to {taxiInstructions.DestinationName}.";
                reply.ContinueProcessing = false;
                return reply;
            }
        }

        private static string ConvertTaxiInstructionsToSsml(TaxiRouter.TaxiInstructions taxiInstructions)
        {
            var spokenInstructions = $"{Random(InstructionsVariants)} {taxiInstructions.DestinationName} ";

            if (taxiInstructions.TaxiwayNames.Count > 0)
                spokenInstructions +=
                    $"<break time=\"60ms\" /> {Random(ViaVariants)}<break time=\"60ms\" /> {string.Join(" <break time=\"60ms\" /> ", taxiInstructions.TaxiwayNames)}";

            if (taxiInstructions.Comments.Count > 0)
                spokenInstructions += $", {string.Join(", ", taxiInstructions.Comments)}";

            return spokenInstructions + ".";
        }

        private static string Random(Array array)
        {
            return array.GetValue(Randomizer.Next(array.Length))?.ToString();
        }
    }
}