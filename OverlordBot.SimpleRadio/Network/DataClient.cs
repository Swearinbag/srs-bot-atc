﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;
using RurouniJones.OverlordBot.SimpleRadio.Models;
using RurouniJones.OverlordBot.SimpleRadio.Network.Util;

namespace RurouniJones.OverlordBot.SimpleRadio.Network
{
    internal class DataClient
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly ClientInformation _clientInformation;
        private readonly RadioInformation _radioInformation;
        private readonly double _controllerFrequency;
        private readonly RadioInformation.Radio.Modulations _controllerModulation;
        private readonly ConcurrentDictionary<string, string> _playersOnFrequency;

        private TcpClient _tcpClient;

        public DataClient(ClientInformation clientInformation, RadioInformation radioInformation, ConcurrentDictionary<string, string> playersOnFrequency)
        {
            _clientInformation = clientInformation;
            _radioInformation = radioInformation;
            _controllerFrequency = _radioInformation.Radios[0].Frequency;
            _controllerModulation = _radioInformation.Radios[0].Modulation;
            _playersOnFrequency = playersOnFrequency;
        }

        public async Task<bool> ConnectAsync(IPEndPoint endpoint, CancellationToken cancellationToken)
        {
            return await Task.Run(async () =>
            {
                _tcpClient = new TcpClient();

               while (!cancellationToken.IsCancellationRequested && !_tcpClient.Connected)
                {
                    _logger.Debug($"Connecting to {endpoint.Address}:{endpoint.Port}");
                    _playersOnFrequency.Clear();
                    try
                    {
                        _tcpClient.SendTimeout = 10000;
                        _tcpClient.ReceiveTimeout = 5000;
                        _tcpClient.NoDelay = true;

                        // Wait for 10 seconds before aborting connection attempt - no SRS server running/port opened in that case
                        await _tcpClient.ConnectAsync(endpoint.Address, endpoint.Port, cancellationToken);
                    }
                    catch (SocketException ex)
                    {
                        _logger.Warn(ex.Message);
                    }
                    catch (OperationCanceledException)
                    {
                        _logger.Debug($"Connection attempts to {endpoint.Address}:{endpoint.Port} cancelled");
                    }
                }

                return true;
            }, CancellationToken.None);
        }

        #region Sending commands

        public async Task<bool> SendInitialSync()
        {
            return await SendMessageToServer(new NetworkMessage
            {
                ClientInformation = _clientInformation,
                MsgType = NetworkMessage.MessageType.Sync
            });
        }

        public async Task SendExternalAwacsRequest(string password)
        {
            await SendMessageToServer(new NetworkMessage
            {
                ClientInformation = _clientInformation,
                ExternalAwacsModePassword = password,
                MsgType = NetworkMessage.MessageType.ExternalAwacsModePassword
            });
        }

        public async Task SendAwacsRadioInformation()
        {
            _clientInformation.RadioInformation = _radioInformation;

            await SendMessageToServer(new NetworkMessage
            {
                ClientInformation = _clientInformation,
                MsgType = NetworkMessage.MessageType.RadioUpdate
            });
        }

        private async Task<bool> SendMessageToServer(NetworkMessage message)
        {
            try
            {
                message.Version = Client.Version;

                var json = message.Encode();

                _logger.Trace($"Sent {message.MsgType}: {json}");

                var bytes = Encoding.UTF8.GetBytes(json);

                await _tcpClient.GetStream().WriteAsync(bytes, 0, bytes.Length, CancellationToken.None);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Processing incoming messages

        public async Task StartClientServerUpdatesAsync(CancellationToken cancellationToken)
        {
            await Task.Run(async () =>
            {
                using var reader = new StreamReader(_tcpClient.GetStream(), Encoding.UTF8);
                while (!cancellationToken.IsCancellationRequested)
                {
                    string line = null;
                    try
                    {
                        // ReSharper disable once MethodHasAsyncOverload
                        line = reader.ReadLine();
                        if (await ProcessMessage(line) == false)
                        {
                            break;
                        }
                    }
                    catch (IOException)
                    {
                        // The TcpClient has a Read timeout set so that we can use ReadLine without blocking the
                        // thread until eternity. The proper solution is to instead use Reader.ReadLineAsync and
                        // pass in the cancellation token but this API doesn't support that yet.  Once this is
                        // implemented we can also get rid of the await Task.Run as well.
                        // See https://github.com/dotnet/runtime/issues/20824 for more information
                    }
                    catch (Exception e)
                    {
                        _logger.Warn(e, $"Error reading line: {line}");
                    }
                }
            }, cancellationToken);
        }

        private async Task<bool> ProcessMessage(string line)
        {
            if (line == null) return true;
            NetworkMessage serverMessage;
            try
            {
                serverMessage = JsonConvert.DeserializeObject<NetworkMessage>(line);
            }
            catch (JsonReaderException ex)
            {
                _logger.Warn(ex,$"Couldn't read otherClient: {line}");
                return true;
            }

            _logger.Trace($"Received {serverMessage.MsgType}: {line}");

            switch (serverMessage.MsgType)
            {
                case NetworkMessage.MessageType.Ping:

                case NetworkMessage.MessageType.ServerSettings:
                case NetworkMessage.MessageType.VersionMismatch:
                case NetworkMessage.MessageType.ExternalAwacsModeDisconnect:
                    break; // We don't care about any of the above!
                case NetworkMessage.MessageType.Sync:
                    ProcessSync(serverMessage);
                    break;
                case NetworkMessage.MessageType.Update:
                case NetworkMessage.MessageType.RadioUpdate:
                    ProcessRadioUpdate(serverMessage.ClientInformation);
                    break;
                case NetworkMessage.MessageType.ClientDisconnect:
                    ProcessClientDisconnect(serverMessage);
                    break;
                case NetworkMessage.MessageType.ExternalAwacsModePassword:
                    if (serverMessage.ClientInformation.Coalition > 0)
                    {
                        _clientInformation.Coalition = serverMessage.ClientInformation.Coalition;
                        await SendAwacsRadioInformation();
                    }
                    else
                    {
                        return false;
                    }
                    break;
                default:
                    _logger.Warn($"Message type not recognized: {line}");
                    break;
            }

            return true;
        }

        private void ProcessSync(NetworkMessage message)
        {
            _logger.Trace($"Processing Synchronisation");
            foreach (var clientInformation in message.Clients)
            {
                _logger.Trace($"Processing Synchronisation for {clientInformation.Name} ({clientInformation.Guid})");
                ProcessRadioUpdate(clientInformation);
            }
        }

        private void ProcessRadioUpdate(ClientInformation otherClient)
        {
            _logger.Trace($"Processing Radio Update");

            var guid = otherClient.Guid;
            var name = otherClient.Name;

            // If a player is in our list then process the update no matter what
            if (_playersOnFrequency.ContainsKey(guid))
            {
                //No-op
            } else { 
                // Otherwise ignore them if they do not match certain characteristics
                if (otherClient.Coalition != _clientInformation.Coalition) return;
                if (otherClient.Guid == _clientInformation.Guid) return;
            }

            var radiosOnFreq = otherClient.RadioInformation?.Radios
                .Where(radio => radio.Frequency.Equals(_controllerFrequency))
                .Count(radio => radio.Modulation.Equals(_controllerModulation));

            if (radiosOnFreq != null && radiosOnFreq > 0 && !_playersOnFrequency.ContainsKey(guid))
            {
                _playersOnFrequency[guid] = name;
                _logger.Trace($"Player {name} ({guid}) added to player list on {_controllerFrequency} {_controllerModulation}");
            } else if(_playersOnFrequency.ContainsKey(guid)) {
                var result = _playersOnFrequency.TryRemove(guid, out _);
                if (result)
                {
                    _logger.Trace($"Player {name} ({guid}) removed from player list on {_controllerFrequency} {_controllerModulation}");
                } else {
                    _logger.Warn($"Could not remove {name} ({guid}) from player list on {_controllerFrequency} {_controllerModulation}");
                }
            } else {
                // No-Op
            }
        }

        private void ProcessClientDisconnect(NetworkMessage message)
        {
            _logger.Trace($"Processing Client Disconnect");
            ProcessRadioUpdate(message.ClientInformation);
        }

        #endregion
    }
}