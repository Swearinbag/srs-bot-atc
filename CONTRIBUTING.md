# Contributing requirements

## For replacement of STT, NLP, TTS components

For anyone who wants to try and replace either the STT, NLP, TTS sections of OverlordBot; here are the following requirements if you want to get them merged.

1. Create / Adopt a Gitlab ticket and tell me in advance what you plan to do so that we can discuss it and not have you waste time doing it a way that will not get merged.
2. The new and old solutions must be able to run side-by-side with the implementation selected at runtime in the config at the Controller level.
3. Submit small commits in ideally the following order: Any refactoring you need to do, then the interfaces (In a separate csproj in the solution), then changing the existing implementation to use those interfaces (These two could be combined if small), then the new implementation & config switch. See here for an example: https://gitlab.com/overlordbot/srs-bot/-/issues/58
4. The usual stuff like following the existing project design and naming conventions. 
5. You will need to do the testing yourself; I can provide keys for Azure resources if you need them.

## For major changes or feature additions

1. Create / Adopt a Gitlab ticket and tell me in advance what you plan to do so that we can discuss it and not have you waste time doing it a way that will not get merged.

## For all other changes

1. Create / Adopt a Gitlab ticket as an option or you can just create the PR and we go from there.