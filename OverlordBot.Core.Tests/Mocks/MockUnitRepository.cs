﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Threading.Tasks;
using Geo.Geometries;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

#pragma warning disable 1998

namespace RurouniJones.OverlordBot.Core.Tests.Mocks
{
    internal class MockUnitRepository : IUnitRepository
    {
        public List<Unit> FindHostileAircraftResult { get; set; } = new();
        public Unit FindAwacsResult{ get; set; } = null;
        public Unit FindByCallsignAndCoalitionResult { get; set; } = new();
        public async Task<List<Unit>> FindHostileAircraft(Point sourcePosition, int coalition, List<string> aircraftCodes, string serverShortName)
        {
            return FindHostileAircraftResult;
        }

        public async Task<Unit> FindAwacs(string pilot, string serverShortName)
        {
            return FindAwacsResult;
        }
        public async Task<Unit> FindByCallsignAndCoalition(string groupName, int flight, int element, int coalition)
        {
            return FindByCallsignAndCoalitionResult;
        }
    }
}
