﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using DEM.Net.Core;
using DEM.Net.Core.Datasets;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NLog;

// ReSharper disable once IdentifierTypo
namespace RurouniJones.OverlordBot.TerrainDownloader
{
    internal class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly DEMDataSet DataSet = DEMDataSet.SRTM_GL1;

        private static IDEMDataSetIndex RasterIndexServiceResolver(DEMDataSourceType dataSourceType)
        {
            return new GDALVRTFileService(null, new Logger<GDALVRTFileService>(new NullLoggerFactory()));
        }

        private static void Main()
        {
            Logger.Info("Initializing Terrain Downloader");
            var rasterService = new RasterService(RasterIndexServiceResolver,
                new Logger<RasterService>(new NullLoggerFactory()));
            Logger.Info($"Downloading to {rasterService.LocalDirectory}");
            var elevationService =
                new ElevationService(rasterService, new Logger<ElevationService>(new NullLoggerFactory()));

            Logger.Info("Downloading Caucasus");
            elevationService.DownloadMissingFiles(DataSet, new BoundingBox(35, 47, 40, 46));

            Logger.Info("Downloading Persian Gulf");
            elevationService.DownloadMissingFiles(DataSet, new BoundingBox(51, 59, 23, 31));

            Logger.Info("Downloading Syria");
            elevationService.DownloadMissingFiles(DataSet, new BoundingBox(29, 42, 31, 38));
        }
    }
}
